package org.ipt.dissertacao.mestrado;

import com.github.javafaker.Faker;

public class ForMutationClass {

	public ForMutationClass () {
		
	}
		
	public int calculateAddition(int a, int b) {
		return a+b;
	}
	
	public int calculateSubtraction(int a, int b) {
		return a-b;
	}
	
	public int calculateNegation(int a) {
		return -a;
	}
	
	public int calculatePositivation(int a) {
		return Math.abs(a);
	}
	
	public int calculateAdditionWithRandomException(int a, int b) throws RuntimeException{
		
		if (Faker.instance().number().numberBetween(0, 10) % 2 == 0)
			throw new RuntimeException("Exception generated randomically");
		
		return calculateAddition(a, b);
	}
	
}
