package org.ipt.dissertacao.mestrado.tests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.log4j.Logger;
import org.ipt.dissertacao.mestrado.ForMutationClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.github.javafaker.Faker;

public class ForMutationTestClass {

	static Logger logger = Logger.getLogger(ForMutationTestClass.class);
	static final int MAX_DIGITS_OF_ENTRY_NUMBERS = 7;
			
	static ForMutationClass forMutationClass;
	
	static final int a = Integer.parseInt(Faker.instance().number().digits(Faker.instance().number().numberBetween(1, MAX_DIGITS_OF_ENTRY_NUMBERS ))),
			   		 b = -Integer.parseInt(Faker.instance().number().digits(Faker.instance().number().numberBetween(1, MAX_DIGITS_OF_ENTRY_NUMBERS )));
	
	@BeforeAll
	public static void test() {
		forMutationClass = new ForMutationClass();
	
		logger.info("Parāmetros: A = " + a);
		logger.info("Parāmetros: B = " + b);
	}
	
	@AfterAll
	public static void cleanUp() {
		forMutationClass = null;
	}
	
	@Test	
	public void validateAddition() {
		
		logger.info ("Calculating if sum of (" + a +") and (" + b + ") equals (" + (a+b) + ")");
		assertTrue(forMutationClass.calculateAddition(a, b) == a+b, "Calculating if sum of (" + a +") and (" + b + ") equals (" + (a+b) + ")");
	}
	
	@Test	
	public void validateAdditionWithExceptionThrownRandomically() {
		
		logger.info("Calculating if sum of (" + a +") and (" + b + ") equals (" + (a+b) + ") with possibility of generate exception");
		//assertTrue(forMutationClass.calculateAdditionWithRandomException(a, b) == a+b, "Calculating if sum of (" + a +") and (" + b + ") equals (" + (a+b) + ") with possibility of generate exception");
	}
	
	@Test	
	public void validateSubtraction() {
		logger.info("Calculating if sum of (" + a +") and (" + b + ") equals (" + (a-b) + ")");
		forMutationClass.calculateSubtraction(a, b);
		//assertTrue(forMutationClass.calculateSubtraction(a, b) == a-b, "Calculating if sum of (" + a +") and (" + b + ") equals (" + (a-b) + ")");
	}
	
	@Test	
	public void validateNegation() {
		logger.info("Calculating if negation of (" + a +") is (" + (-a) + ")");
		assertTrue(forMutationClass.calculateNegation(a) == -a, "Calculating if negation of (" + a +") is (" + (-a) + ")");
	}
	
	@Test	
	public void validatePositivation() {
		logger.info("Calculating if positivation of (" + b +") is (" + (Math.abs(b)) + ")");
		assertTrue(forMutationClass.calculatePositivation(b) == Math.abs(b), "Calculating if positivation of (" + b +") is (" + (Math.abs(b)) + ")");
	}
}
